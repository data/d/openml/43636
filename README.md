# OpenML dataset: Facebook-Complete-Stock-Data2012---2020Latest

https://www.openml.org/d/43636

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Facebook is a company that literally every kid is aware of. Its a household name. People from various age groups are there on this social media website. It has helped many in connecting with different people and also has helped some of the investors by earning them a good amount of money. This data set contains the details of the stock of Facebook Inc.
Content
This data set has 7 columns with all the necessary values such as opening price of the stock, the closing price of it, its highest in the day and much more. It has date wise data of the stock starting from 2012 to 2020(August).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43636) of an [OpenML dataset](https://www.openml.org/d/43636). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43636/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43636/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43636/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

